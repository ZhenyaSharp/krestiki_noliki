﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace krestiki_noliki
{
    class Program
    {
        enum Cell
        {
            Empty,
            Krestik,
            Nolik
        }

        enum Step
        {
            Player1,
            Player2,
            Draw
        }
        static void Main(string[] args)
        {

            #region Создание переменных и игрового поля

            int numCellI, numCellJ;
            int fieldSize = 3;
            bool playGame = true;
            bool inputResultNumI, inputResultNumJ;
            Step currentStep = Step.Player1;
            Step winner = Step.Player1;
            int countStep=0;

            Cell[,] gameField = new Cell[fieldSize, fieldSize];

            #endregion

            #region Заполнение игрового поля пустотой

            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    gameField[i, j] = Cell.Empty;
                }
            }
            #endregion

            #region  Игровой цикл

            #region Вывод пустого игрового поля на экран и проверка на ничью
                    while (playGame)
                    {
                        Console.Clear();

                Console.WriteLine("***КРЕСТИКИ - НОЛИКИ***");

                        for (int i = 0; i < fieldSize; i++)
                        {
                            for (int j = 0; j < fieldSize; j++)
                            {
                                switch (gameField[i, j])
                                {
                                    case Cell.Empty:
                                        Console.Write(".");
                                        break;
                                    case Cell.Krestik:
                                        Console.Write("X");
                                        break;
                                    case Cell.Nolik:
                                        Console.Write("O");
                                        break;
                                }
                            }
                            Console.WriteLine();
                        }


                #endregion

            #region Ходы игроков с проверкой ввода и передачей хода + проверка на победителя/ничью


                if (countStep == 9)
                {
                    playGame = false;
                    winner = Step.Draw;
                    break;
                }

                switch (currentStep)
                {
                    case Step.Player1:
                        Console.WriteLine("Игрок №1 ваш ход: ");

                        do
                        {
                            Console.Write("Введите номер строчки: ");
                            inputResultNumI = int.TryParse(Console.ReadLine(), out numCellI);

                            Console.Write("Введите номер столбца: ");
                            inputResultNumJ = int.TryParse(Console.ReadLine(), out numCellJ);

                        } while (inputResultNumI == false || inputResultNumJ == false || numCellI - 1 < 0 || numCellI - 1 > fieldSize - 1 || numCellJ - 1 < 0 ||
                        numCellJ - 1 > fieldSize - 1 || gameField[numCellI - 1, numCellJ - 1] == Cell.Krestik || gameField[numCellI - 1, numCellJ - 1] == Cell.Nolik);

                        gameField[numCellI - 1, numCellJ - 1] = Cell.Krestik;

                        if (gameField[0, 0] == Cell.Krestik && gameField[0, 1] == Cell.Krestik && gameField[0, 2] == Cell.Krestik || gameField[0, 0] == Cell.Krestik && gameField[1, 0] == Cell.Krestik && gameField[2, 0] == Cell.Krestik ||
                            gameField[0, 1] == Cell.Krestik && gameField[1, 1] == Cell.Krestik && gameField[2, 1] == Cell.Krestik || gameField[0, 2] == Cell.Krestik && gameField[1, 2] == Cell.Krestik && gameField[2, 2] == Cell.Krestik ||
                            gameField[1, 0] == Cell.Krestik && gameField[1, 1] == Cell.Krestik && gameField[1, 2] == Cell.Krestik || gameField[2, 0] == Cell.Krestik && gameField[2, 1] == Cell.Krestik && gameField[2, 2] == Cell.Krestik ||
                            gameField[0, 0] == Cell.Krestik && gameField[1, 1] == Cell.Krestik && gameField[2, 2] == Cell.Krestik || gameField[0, 2] == Cell.Krestik && gameField[1, 1] == Cell.Krestik && gameField[2, 0] == Cell.Krestik)
                        {
                            playGame = false;
                            winner = currentStep;
                            break;
                        }
                        else
                        {
                            currentStep = Step.Player2;
                            break;
                        }

                    case Step.Player2:
                        Console.WriteLine("Игрок №2 ваш ход: ");

                        do
                        {
                            Console.Write("Введите номер строчки: ");
                            inputResultNumI = int.TryParse(Console.ReadLine(), out numCellI);

                            Console.Write("Введите номер столбца: ");
                            inputResultNumJ = int.TryParse(Console.ReadLine(), out numCellJ);

                        } while (inputResultNumI == false || inputResultNumJ == false || numCellI - 1 < 0 || numCellI - 1 > fieldSize - 1 || numCellJ - 1 < 0 ||
                        numCellJ - 1 > fieldSize - 1 || gameField[numCellI - 1, numCellJ - 1] == Cell.Krestik || gameField[numCellI - 1, numCellJ - 1] == Cell.Nolik);

                        gameField[numCellI - 1, numCellJ - 1] = Cell.Nolik;

                        if (gameField[0, 0] == Cell.Nolik && gameField[0, 1] == Cell.Nolik && gameField[0, 2] == Cell.Nolik || gameField[0, 0] == Cell.Nolik && gameField[1, 0] == Cell.Nolik && gameField[2, 0] == Cell.Nolik ||
                            gameField[0, 1] == Cell.Nolik && gameField[1, 1] == Cell.Nolik && gameField[2, 1] == Cell.Nolik || gameField[0, 2] == Cell.Nolik && gameField[1, 2] == Cell.Nolik && gameField[2, 2] == Cell.Nolik ||
                            gameField[1, 0] == Cell.Nolik && gameField[1, 1] == Cell.Nolik && gameField[1, 2] == Cell.Nolik || gameField[2, 0] == Cell.Nolik && gameField[2, 1] == Cell.Nolik && gameField[2, 2] == Cell.Nolik ||
                            gameField[0, 0] == Cell.Nolik && gameField[1, 1] == Cell.Nolik && gameField[2, 2] == Cell.Nolik || gameField[0, 2] == Cell.Nolik && gameField[1, 1] == Cell.Nolik && gameField[2, 0] == Cell.Nolik)
                        {
                            playGame = false;
                            winner = currentStep;
                            break;
                        }
                        else
                        {
                            currentStep = Step.Player1;
                            break;
                        }
                        
                }
                countStep++;
            }
            #endregion

            #endregion

            #region Вывод итога игры

            Console.Clear();

            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    switch (gameField[i, j])
                    {
                        case Cell.Empty:
                            Console.Write(".");
                            break;
                        case Cell.Krestik:
                            Console.Write("X");
                            break;
                        case Cell.Nolik:
                            Console.Write("O");
                            break;
                    }
                }
                Console.WriteLine();
            }

            switch (winner)
            {
                case Step.Player1:
                    Console.WriteLine($"Первый игрок победил!");
                    break;
                case Step.Player2:
                    Console.WriteLine($"Второй игрок победил!");
                    break;
                case Step.Draw:
                    Console.WriteLine("Ничья!");
                    break;

            }
            #endregion

            Console.ReadKey();

            }
    }
}

